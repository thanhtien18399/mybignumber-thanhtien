
function sum(a,b){
    let res="";
    let miss=0;
    while(a.length<b.length) a="0" + a;
    while(a.length>b.length) b="0" + b;
    let len=a.length-1
    for(let i=len;i>=0;i--){
        let number=parseInt(a[i])+parseInt(b[i])+miss;
        res = `${number%10}`+res;
        miss= Math.floor(number/10);
    }
    return res;
}
function resultClick(){
    let stra=document.getElementById('numberA').value;
    let strb=document.getElementById('numberB').value;
    if(!validation(stra)||!validation(strb)){
        document.querySelector('.err').innerHTML=`<span> <i class="fa-solid fa-circle-exclamation"></i> Please enter a positive integer !!!! <br></span>
        <span class="example">Example:<br> Number 1: 123 <br>Number 2: 789</span>
        `
        return 0;
    }
    document.querySelector('.err').innerHTML="";
    document.querySelector('.res').innerHTML=sum(stra,strb)
    
}
function validation(str){
   let number= /^\d+$/.test(str);
   return number;
}
