*Viết hàm cài đặt thuật toán cộng 2 số lớn.
*Thay vì tính trên giấy thì em viết hàm này để tính được công 2 số lớn rồi in ra kết quả cho người dùng.
*Em dùng ngôn ngữ  Javascript để viết hàm. Ngoài ra em còn dùng thư viện Bootstrap , font gg , fontawesome để xử lí giao diện.
    + Láy dữ liệu từ thẻ input bên HTML để xử lý. Dùng DOM.
    + Validation sử dụng biểu thức chính quy Regex dùng methods text().
    + Hàm Sum() trong đó em nhận dữ liệu từ người dùng nhập vào gồm strA và strB.
        - Dùng While để thêm phần tử bị thiếu vào strA và strB.
        - Có được dữ liệu thì dùng vòng lặp for để chạy tính cộng và lưu vào chuỗi mới.
        - Khi vòng lặp chạy xong ta có được chuỗi kết quả và in ra giao diện.
*Cách Sử Dụng:
    + Chạy file index.html sẽ hiện lên giao diện cho người dùng nhập liệu.
    + Giao diện gồm có :
        - Hai input để nhập số nguyên dương. Nếu nhập không đúng định dạng thì không trả về kết quả mà thay vào đó là một ví dụ sẽ hiện lên dưới thẻ Input.
        - Người dùng nhập đúng định dạng.
        - Button result sẽ gửi đi dữ liệu và chạy hàm sum().
        - In ra kết quả phía dưới.
*Cảm ơn bạn đã xem !!!